var express = require('express');
var router = express.Router();
const clienteController = require('../controllers').clienteController;
const reglaController = require('../controllers').reglaController;
const conceptoController = require('../controllers').conceptoController;
const bolsaController = require('../controllers').bolsaController;
const parametrizacionController = require('../controllers').parametrizacionController;
const cabeceraController = require('../controllers').cabeceraController;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
/*cliente */
router.get('/api/cliente',clienteController.list);
router.get('/api/cliente/:id',clienteController.getById);
router.post('/api/cliente', clienteController.add2);
router.delete('/api/cliente/:id',clienteController.delete);
router.put('/api/cliente/:id',clienteController.update);
/*regla */
router.get('/api/regla',reglaController.list);
router.get('/api/regla/punto',reglaController.getPuntos);
router.get('/api/regla/:id',reglaController.getById);
router.post('/api/regla', reglaController.add2);
router.delete('/api/regla/:id',reglaController.delete);
router.put('/api/regla/:id',reglaController.update);
/*concepto*/
router.get('/api/concepto',conceptoController.list);
router.get('/api/concepto/:id',conceptoController.getById);
router.post('/api/concepto', conceptoController.add2);
router.delete('/api/concepto/:id',conceptoController.delete);
router.put('/api/concepto/:id',conceptoController.update);

/*bolsa*/
router.get('/api/bolsa',bolsaController.getWithParams);
router.get('/api/bolsa/:id',bolsaController.getById);
router.post('/api/bolsa', bolsaController.add2);
router.delete('/api/bolsa/:id',bolsaController.delete);
router.put('/api/bolsa/:id',bolsaController.update);

/*parametrizacion*/
router.get('/api/parametrizacion',parametrizacionController.list);
router.get('/api/parametrizacion/:id',parametrizacionController.getById);
router.post('/api/parametrizacion', parametrizacionController.add2);
router.delete('/api/parametrizacion/:id',parametrizacionController.delete);
router.put('/api/parametrizacion/:id',parametrizacionController.update);

/*cabecera */
router.get('/api/cabecera',cabeceraController.getWithParams);
router.get('/api/cabecera/:id',cabeceraController.getById);
router.post('/api/cabecera', cabeceraController.add2);
router.delete('/api/cabecera/:id',cabeceraController.delete);
router.put('/api/cabecera/:id',cabeceraController.update);


module.exports = router;

const clienteController = require ('./cliente');
const reglaController = require ('./regla');
const conceptoController = require ('./concepto');
const bolsaController = require ('./bolsa');
const parametrizacionController = require ('./parametrizacion');
const cabeceraController = require ('./cabecera');
const detalleController = require ('./detalle');
module.exports = {
    clienteController,
    reglaController,
    conceptoController,
    bolsaController,
    parametrizacionController,
    cabeceraController,
    detalleController,
}

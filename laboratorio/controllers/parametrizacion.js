const Parametrizacion = require('../models').Parametrizacion;
const models = require('../models');

module.exports = {
    list(req,res) {
        return Parametrizacion.findAll()
            .then(
                (parametrizacion) => res.status(200).send(parametrizacion))
            .catch(
                (error) => res.status(400).send(error)
            )
    },
    getById (req,res) {
        return Parametrizacion.findByPk(req.params.id,{})
            .then((parametrizacion) => {
                if (!parametrizacion) {
                    return res.status(404).send(
                        {message:'Parametrizacion no encontrada'}
                        );
                }
                return res.status(200).send(parametrizacion);
            })
            .catch((error) =>
                res.status(400).send(error)
            );
    },
    add2 (req,res) {
        return Parametrizacion.create({
            id: req.body.id,
            fecha_inicio: req.body.fecha_inicio,
            fecha_fin: req.body.fecha_fin,
            duracion: req.body.duracion
        }).then ((parametrizacion) => res.status(200).send(parametrizacion))
            .catch((error) => res.status(400).send(error));
    },
    delete (req, res) {
        return Parametrizacion.findByPk(req.params.id)
            .then ( parametrizacion => {
                if (!parametrizacion) {
                    return res.status(404).send(
                        {message:'Persona no encontrada'}
                    );
                }
                return parametrizacion.destroy()
                    .then( () => res.status(202).send())
                    .catch( (error) => res.status(400).send(error) )
            });
    },
    update (req,res) {
        return Parametrizacion.findByPk(req.params.id)
            .then ( parametrizacion => {
                if (!parametrizacion) {
                    return res.status(404).send(
                        {message:'Persona no encontrada'}
                    );
                }
                return parametrizacion.update({
                    fecha_inicio: req.body.fecha_inicio,
                    fecha_fin: req.body.fecha_fin,
                    duracion: req.body.duracion
                }).then( () => res.status(200).send())
                    .catch( (error) => res.status(400).send(error) )
            });
    },
}

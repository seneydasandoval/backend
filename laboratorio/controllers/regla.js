const Regla = require('../models').Regla;
const models = require('../models');

module.exports = {
    /*saludo (req, res) {
        res.status(200).send("hola mundo");
    }*/
    list(req,res) {
        return Regla.findAll()
            .then(
                (reglas) => res.status(200).send(reglas))
            .catch(
                (error) => res.status(400).send(error)
            )
    },
    getById (req,res) {
        return Regla.findByPk(req.params.id,{})
            .then((regla) => {
                if (!regla) {
                    return res.status(404).send(
                        {message:'Regla no encontrada'}
                        );
                }
                return res.status(200).send(regla);
            })
            .catch((error) =>
                res.status(400).send(error)
            );
    },
    add2 (req,res) {
        return Regla.create({
            id: req.body.id,
            limite_inferior: req.body.limite_inferior,
            limite_superior: req.body.limite_superior,
            monto_equivalente: req.body.monto_equivalente
        }).then ((regla) => res.status(200).send(regla))
            .catch((error) => res.status(400).send(error));
    },
    delete (req, res) {
        return Regla.findByPk(req.params.id)
            .then ( regla => {
                if (!regla) {
                    return res.status(404).send(
                        {message:'Regla no encontrada'}
                    );
                }
                return regla.destroy()
                    .then( () => res.status(202).send())
                    .catch( (error) => res.status(400).send(error) )
            });
    },
    update (req,res) {
        return Regla.findByPk(req.params.id)
            .then ( regla => {
                if (!regla) {
                    return res.status(404).send(
                        {message:'Regla no encontrada'}
                    );
                }
                return regla.update({
                    limite_inferior: req.body.limite_inferior,
                    limite_superior: req.body.limite_superior,
                    monto_equivalente: req.body.monto_equivalente
                }).then( () => res.status(200).send())
                    .catch( (error) => res.status(400).send(error) )

            });
    },
    getPuntos(req,res) {
        var query_select = " select FLOOR(coalesce(" + req.query.monto + ",0)/coalesce(r.monto_equivalente,1))  from regla r";
        console.log("query_select " + query_select);
        return models.sequelize.query(query_select,
            {type: models.Sequelize.QueryTypes.SELECT}
            )
            .then(
                (bolsa) => res.status(200).send(bolsa))
            .catch(
                (error) => res.status(400).send(error)
            )
    },

}
const Cabecera = require('../models').Cabecera;
const models = require('../models');
var nodemailer = require('nodemailer'); 

module.exports = {
    list(req,res) {
        return Cabecera.findAll()
            .then(
                (cabeceras) => res.status(200).send(cabeceras))
            .catch(
                (error) => res.status(400).send(error)
            )
    },
    getById (req,res) {
        return Cabecera.findByPk(req.params.id,{})
            .then((cabecera) => {
                if (!cabecera) {
                    return res.status(404).send(
                        {message:'Cabecera no encontrada'}
                        );
                }
                return res.status(200).send(cabecera);
            })
            .catch((error) =>
                res.status(400).send(error)
            );
    },
    add2 (req,res) {
          var transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            requireTLS: true,
            auth: {
              user: 'ingresapy@gmail.com',
              pass: 'ingresapy2018'
            }
          });
          
          var mailOptions = {
            from: 'ingresapy@gmail.com',
            to: 'josue.trepowski@gmail.com',
            subject: 'Uso de puntos',
            text: 'Ha usado sus puntos satisfactoriamente'
          };
          
          transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
          });
        return Cabecera.create({
            id: req.body.id,
            id_cliente: req.body.id_cliente,
            puntaje_utilizado: req.body.puntaje_utilizado,
            fecha: req.body.fecha,
            id_concepto: req.body.id_concepto
        }).then ((cabecera) => res.status(200).send(cabecera))
            .catch((error) => res.status(400).send(error));
    },
    delete (req, res) {
        return Cabecera.findByPk(req.params.id)
            .then ( cabecera => {
                if (!cabecera) {
                    return res.status(404).send(
                        {message:'Cabecera no encontrada'}
                    );
                }
                return cabecera.destroy()
                    .then( () => res.status(202).send())
                    .catch( (error) => res.status(400).send(error) )
        });
    },
    update (req,res) {
        return Cabecera.findByPk(req.params.id)
            .then ( cabecera => {
                if (!cabecera) {
                    return res.status(404).send(
                        {message:'Cabecera no encontrado'}
                    );
                }
                return cabecera.update({
                    descripcion: req.body.descripcion,
                    puntos: req.body.puntos, 
                }).then( () => res.status(200).send())
                    .catch( (error) => res.status(400).send(error) )

            });
    },
    getWithParams(req,res) {
        var query_select = "select * from (SELECT c.id, c.id_cliente, c.puntaje_utilizado, c.fecha, " +
        "c.id_concepto, co.descripcion as descripcion_concepto, " +
        "cl.nombre as nombre_cliente " +
        "FROM public.cabecera c  " +
        "inner join concepto co on c.id_concepto = co.id " +
        "inner join cliente cl on c.id_cliente = cl.id) t where 1=1 ";
        if (req.query.idCliente!= null) {
            query_select = query_select + " and t.id_cliente = " + req.query.idCliente;
        }
        if (req.query.fechaDesde!= null) {
                query_select = query_select + " and t.fecha >= to_date('" + req.query.fechaDesde + "','yyyy-mm-dd') ";
        }
        if (req.query.fechaHasta!= null) {
                query_select = query_select + " and t.fecha <= to_date('" + req.query.fechaHasta + "','yyyy-mm-dd') ";;
        }
        if (req.query.idConcepto!= null) {
            query_select = query_select + " and t.id_concepto = " + req.query.idConcepto;
        }
        console.log("query_select " + query_select);
        return models.sequelize.query(query_select,
            {type: models.Sequelize.QueryTypes.SELECT}
            )
            .then(
                (cabecera) => res.status(200).send(cabecera))
            .catch(
                (error) => res.status(400).send(error)
            )
    },
}

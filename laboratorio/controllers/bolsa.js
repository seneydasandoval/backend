const Bolsa = require('../models').Bolsa;
const models = require('../models');

module.exports = {
    list(req,res) {
        return Bolsa.findAll()
            .then(
                (bolsas) => res.status(200).send(bolsas))
            .catch(
                (error) => res.status(400).send(error)
            )
    },
    getById (req,res) {
        return Bolsa.findByPk(req.params.id,{})
            .then((bolsa) => {
                if (!bolsa) {
                    return res.status(404).send(
                        {message:'Bolsa no encontrada'}
                        );
                }
                return res.status(200).send(bolsa);
            })
            .catch((error) =>
                res.status(400).send(error)
            );
    },
    add2 (req,res) {
        return Bolsa.create({
            id: req.body.id,
            id_cliente: req.body.id_cliente,
            fecha_asignacion: req.body.fecha_asignacion,
            fecha_caducidad: req.body.fecha_caducidad,
            puntaje_asignado: req.body.puntaje_asignado,
            puntaje_utilizado: req.body.puntaje_utilizado,
            saldo_puntos: req.body.saldo_puntos,
            monto_operacion: req.body.monto_operacion
        }).then ((bolsa) => res.status(200).send(bolsa))
            .catch((error) => res.status(400).send(error));
    },
    delete (req, res) {
        return Bolsa.findByPk(req.params.id)
            .then ( bolsa => {
                if (!bolsa) {
                    return res.status(404).send(
                        {message:'Bolsa no encontrada'}
                    );
                }
                return bolsa.destroy()
                    .then( () => res.status(202).send())
                    .catch( (error) => res.status(400).send(error) )
            });
    },
    update (req,res) {
        return Bolsa.findByPk(req.params.id)
            .then ( bolsa => {
                if (!bolsa) {
                    return res.status(404).send(
                        {message:'Bolsa no encontrada'}
                    );
                }
                return bolsa.update({
                    id_cliente: req.body.id_cliente,
                    fecha_asignacion: req.body.fecha_asignacion,
                    fecha_caducidad: req.body.fecha_caducidad,
                    puntaje_asignado: req.body.puntaje_asignado,
                    puntaje_utilizado: req.body.puntaje_utilizado,
                    saldo_puntos: req.body.saldo_puntos,
                    monto_operacion: req.body.monto_operacion
                }).then( () => res.status(200).send())
                    .catch( (error) => res.status(400).send(error) )

            });
    },
    getWithParams(req,res) {
        var query_select = " select * from (SELECT bo.id, id_cliente, cl.nombre as cliente_nombre, " +
        "fecha_asignacion, coalesce(fecha_caducidad,NOW()) as fecha_caducidad, puntaje_asignado, puntaje_utilizado, " +
         "saldo_puntos, monto_operacion,  EXTRACT(DAY FROM coalesce(fecha_caducidad,NOW()) -NOW()) " +
         "as cantidad_dias  FROM public.bolsa bo " +
        "inner join cliente cl on bo.id_cliente = cl.id) t where 1 = 1 ";
        if (req.query.idCliente!= null) {
            query_select = query_select + " and t.id_cliente = " + req.query.idCliente;
        }
        if (req.query.vencido!= null) {
            if(req.query.vencido.toString() == "S") {
                query_select = query_select + " and t.fecha_caducidad < now() ";
            }
            if(req.query.vencido.toString() == "N") {
                query_select = query_select + " and t.fecha_caducidad >= now() ";
            }
        }
        if (req.query.cantidad!= null) {
            console.log("vencido " + req.query.cantidad);
            query_select = query_select + " and t.cantidad_dias = " + req.query.cantidad;
        }
        console.log("query_select " + query_select);
        return models.sequelize.query(query_select,
            {type: models.Sequelize.QueryTypes.SELECT}
            )
            .then(
                (bolsa) => res.status(200).send(bolsa))
            .catch(
                (error) => res.status(400).send(error)
            )
    },
}

const Concepto = require('../models').Concepto;
const models = require('../models');

module.exports = {
    list(req,res) {
        return Concepto.findAll()
            .then(
                (conceptos) => res.status(200).send(conceptos))
            .catch(
                (error) => res.status(400).send(error)
            )
    },
    getById (req,res) {
        return Concepto.findByPk(req.params.id,{})
            .then((concepto) => {
                if (!concepto) {
                    return res.status(404).send(
                        {message:'Concepto no encontrada'}
                        );
                }
                return res.status(200).send(concepto);
            })
            .catch((error) =>
                res.status(400).send(error)
            );
    },
    add2 (req,res) {
        return Concepto.create({
            id: req.body.id,
            descripcion: req.body.descripcion,
            puntos: req.body.puntos,  
        }).then ((concepto) => res.status(200).send(concepto))
            .catch((error) => res.status(400).send(error));
    },
    delete (req, res) {
        return Concepto.findByPk(req.params.id)
            .then ( concepto => {
                if (!concepto) {
                    return res.status(404).send(
                        {message:'Concepto no encontrada'}
                    );
                }
                return concepto.destroy()
                    .then( () => res.status(202).send())
                    .catch( (error) => res.status(400).send(error) )
        });
    },
    update (req,res) {
        return Concepto.findByPk(req.params.id)
            .then ( concepto => {
                if (!concepto) {
                    return res.status(404).send(
                        {message:'Concepto no encontrado'}
                    );
                }
                return concepto.update({
                    descripcion: req.body.descripcion,
                    puntos: req.body.puntos, 
                }).then( () => res.status(200).send())
                    .catch( (error) => res.status(400).send(error) )

            });
    },
}

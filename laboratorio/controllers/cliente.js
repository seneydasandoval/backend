const Cliente = require('../models').Cliente;
const models = require('../models');

module.exports = {
    /*saludo (req, res) {
        res.status(200).send("hola mundo");
    }*/
    list(req,res) {
        return Cliente.findAll()
            .then(
                (clientes) => res.status(200).send(clientes))
            .catch(
                (error) => res.status(400).send(error)
            )
    },
    getById (req,res) {
        return Cliente.findByPk(req.params.id,{})
            .then((cliente) => {
                if (!cliente) {
                    return res.status(404).send(
                        {message:'Persona no encontrada'}
                        );
                }
                return res.status(200).send(cliente);
            })
            .catch((error) =>
                res.status(400).send(error)
            );
    },
    add2 (req,res) {
        return Cliente.create({
            id: req.body.id,
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            nro_documento: req.body.nro_documento,
            tipo_documento: req.body.tipo_documento,
            nacionalidad: req.body.nacionalidad,
            email: req.body.email,
            telefono: req.body.telefono,
            fecha_nac: req.body.fecha_nac     
        }).then ((cliente) => res.status(200).send(cliente))
            .catch((error) => res.status(400).send(error));
    },
    delete (req, res) {
        return Cliente.findByPk(req.params.id)
            .then ( cliente => {
                if (!cliente) {
                    return res.status(404).send(
                        {message:'Persona no encontrada'}
                    );
                }
                return cliente.destroy()
                    .then( () => res.status(202).send())
                    .catch( (error) => res.status(400).send(error) )
            });
    },
    update (req,res) {
        return Cliente.findByPk(req.params.id)
            .then ( cliente => {
                if (!cliente) {
                    return res.status(404).send(
                        {message:'Persona no encontrada'}
                    );
                }
                return cliente.update({
                    nombre: req.body.nombre,
                    apellido: req.body.apellido,
                    nro_documento: req.body.nro_documento,
                    tipo_documento: req.body.tipo_documento,
                    nacionalidad: req.body.nacionalidad,
                    email: req.body.email,
                    telefono: req.body.telefono,
                    fecha_nac: req.body.fecha_nac  
                }).then( () => res.status(200).send())
                    .catch( (error) => res.status(400).send(error) )

            });
    },
}

const Detalle = require('../models').Detalle;
const models = require('../models');

module.exports = {
    list(req,res) {
        return Detalle.findAll()
            .then(
                (detalles) => res.status(200).send(detalles))
            .catch(
                (error) => res.status(400).send(error)
            )
    },
    getById (req,res) {
        return Detalle.findByPk(req.params.id,{})
            .then((detalle) => {
                if (!detalle) {
                    return res.status(404).send(
                        {message:'Detalle no encontrada'}
                        );
                }
                return res.status(200).send(detalle);
            })
            .catch((error) =>
                res.status(400).send(error)
            );
    },
    add2 (req,res) {
        return Detalle.create({
            id: req.body.id,
            id_cabecera: req.body.id_cabecera,
            puntaje_utilizado: req.body.puntaje_utilizado,
            id_bolsa: req.body.id_bolsa
        }).then ((detalle) => res.status(200).send(detalle))
            .catch((error) => res.status(400).send(error));
    },
    delete (req, res) {
        return Detalle.findByPk(req.params.id)
            .then ( detalle => {
                if (!detalle) {
                    return res.status(404).send(
                        {message:'Detalle no encontrada'}
                    );
                }
                return detalle.destroy()
                    .then( () => res.status(202).send())
                    .catch( (error) => res.status(400).send(error) )
        });
    },
    update (req,res) {
        return Detalle.findByPk(req.params.id)
            .then ( detalle => {
                if (!detalle) {
                    return res.status(404).send(
                        {message:'Detalle no encontrado'}
                    );
                }
                return detalle.update({
                    id_cabecera: req.body.id_cabecera,
                    puntaje_utilizado: req.body.puntaje_utilizado,
                    id_bolsa: req.body.id_bolsa
                }).then( () => res.status(200).send())
                    .catch( (error) => res.status(400).send(error) )

            });
    },
}

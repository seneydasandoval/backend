'use strict';
module.exports = (sequelize, DataTypes) => {
  const Regla = sequelize.define('Regla', {
    id: {type:DataTypes.INTEGER,primaryKey: true,autoIncrement:true},
    limite_inferior: DataTypes.DOUBLE,
    limite_superior: DataTypes.DOUBLE,
    monto_equivalente: DataTypes.DOUBLE
  }, {tableName:'regla',timestamps:false});
  Regla.associate = function(models) {
    // associations can be defined here
  };
  return Regla;
};
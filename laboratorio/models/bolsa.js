'use strict';
module.exports = (sequelize, DataTypes) => {
  const Bolsa = sequelize.define('Bolsa', {
    id: {type:DataTypes.INTEGER,primaryKey: true,autoIncrement:true},
    id_cliente: DataTypes.INTEGER,
    fecha_asignacion: DataTypes.DATE,
    fecha_caducidad: DataTypes.DATE,
    puntaje_asignado: DataTypes.INTEGER,
    puntaje_utilizado: DataTypes.INTEGER,
    saldo_puntos: DataTypes.INTEGER,
    monto_operacion: DataTypes.DOUBLE
  }, {tableName:'bolsa',timestamps:false});
  Bolsa.associate = function(models) {
    // associations can be defined here
  };
  return Bolsa;
};
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Detalle = sequelize.define('Detalle', {
    id: {type:DataTypes.INTEGER,primaryKey: true,autoIncrement:true},
    id_cabecera: DataTypes.INTEGER,
    puntaje_utilizado: DataTypes.INTEGER,
    id_bolsa: DataTypes.INTEGER
  }, {tableName:'detalle',timestamps:false});
  Detalle.associate = function(models) {
    // associations can be defined here
  };
  return Detalle;
};
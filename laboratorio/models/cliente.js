'use strict';
module.exports = (sequelize, DataTypes) => {
  const Cliente = sequelize.define('Cliente', {
    id: {type:DataTypes.INTEGER,primaryKey: true,autoIncrement:true},
    nombre: DataTypes.STRING,
    apellido: DataTypes.STRING,
    nro_documento: DataTypes.STRING,
    tipo_documento: DataTypes.STRING,
    nacionalidad: DataTypes.STRING,
    email: DataTypes.STRING,
    telefono: DataTypes.STRING,
    fecha_nac: DataTypes.DATE
  }, {tableName:'cliente',timestamps:false});
  Cliente.associate = function(models) {
    // associations can be defined here
  };
  return Cliente;
};
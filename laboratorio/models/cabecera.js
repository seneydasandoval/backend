'use strict';
module.exports = (sequelize, DataTypes) => {
  const Cabecera = sequelize.define('Cabecera', {
    id: {type:DataTypes.INTEGER,primaryKey: true,autoIncrement:true},
    id_cliente: DataTypes.INTEGER,
    puntaje_utilizado: DataTypes.INTEGER,
    fecha: DataTypes.DATE,
    id_concepto: DataTypes.INTEGER
  }, {tableName:'cabecera',timestamps:false});
  Cabecera.associate = function(models) {
    // associations can be defined here
  };
  return Cabecera;
};
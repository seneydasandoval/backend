'use strict';
module.exports = (sequelize, DataTypes) => {
  const Concepto = sequelize.define('Concepto', {
    id: {type:DataTypes.INTEGER,primaryKey: true,autoIncrement:true},
    descripcion: DataTypes.STRING,
    puntos: DataTypes.INTEGER,
  }, {tableName:'concepto',timestamps:false});
  Concepto.associate = function(models) {
    // associations can be defined here
  };
  return Concepto;
};
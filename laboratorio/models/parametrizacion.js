'use strict';
module.exports = (sequelize, DataTypes) => {
  const Parametrizacion = sequelize.define('Parametrizacion', {
    id: {type:DataTypes.INTEGER,primaryKey: true,autoIncrement:true},
    fecha_inicio: DataTypes.DATE,
    fecha_fin: DataTypes.DATE,
    duracion: DataTypes.INTEGER
  }, {tableName:'parametrizacion',timestamps:false});
  Parametrizacion.associate = function(models) {
    // associations can be defined here
  };
  return Parametrizacion;
};
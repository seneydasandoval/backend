'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('bolsa', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_cliente: {
        type: Sequelize.INTEGER
      },
      fecha_asignacion: {
        type: Sequelize.DATE
      },
      fecha_caducidad: {
        type: Sequelize.DATE
      },
      puntaje_asignado: {
        type: Sequelize.INTEGER
      },
      puntaje_utilizado: {
        type: Sequelize.INTEGER
      },
      saldo_puntos: {
        type: Sequelize.INTEGER
      },
      monto_operacion: {
        type: Sequelize.DOUBLE
      },
      createdAt: {
        type: Sequelize.DATE
      },
      updatedAt: {
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('bolsa');
  }
};
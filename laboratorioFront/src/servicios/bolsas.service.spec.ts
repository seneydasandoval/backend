import { TestBed } from '@angular/core/testing';

import { BolsasService } from './bolsas.service';

describe('BolsasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BolsasService = TestBed.get(BolsasService);
    expect(service).toBeTruthy();
  });
});

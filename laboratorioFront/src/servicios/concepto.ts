export interface Concepto {
    id: number,
    descripcion: string,
    puntos: number,
}

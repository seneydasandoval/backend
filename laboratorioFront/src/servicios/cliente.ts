export interface Cliente {
    id: number,
    nombre: string,
    apellido: string
    nro_documento: string,
    tipo_documento: string,
    nacionalidad: string,
    email: string,
    telefono: string,
    fecha_nac: Date,
}

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {environment} from "../environments/environment";
import { HttpParams, HttpClient } from '@angular/common/http';
import { map, retry, catchError } from "rxjs/operators";
import { Bolsa } from './bolsa';
import { Http, Response, Headers, RequestOptions } from '@angular/http';    

@Injectable({
  providedIn: 'root'
})
export class BolsasService {
  private basePath = 'http://localhost:3000/api/bolsa/';

  private headers = new Headers({ 'Content-Type': 'application/json' });  
  // Define API
  apiURL = 'http://localhost:3000/api';
  
  // For Using Fake API by Using It's URL  
  constructor(private http: HttpClient) {}
   
  private basePath1: string="/api/bolsa/";

  // HttpClient API get() method => Fetch employees list
   getBolsas(): Observable<Bolsa> {
    return this.http.get<Bolsa>(this.apiURL + '/bolsa/')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  // HttpClient API get() method => Fetch employee
  getBolsaById(id:number): Observable<Bolsa> {
    return this.http.get<Bolsa>(this.apiURL + '/bolsa/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  
  // HttpClient API put() method => Update employee
  updateBolsa(id: number, bolsa: Bolsa): Observable<Bolsa> {
    return this.http.put<Bolsa>(this.apiURL + '/bolsa/' + id,bolsa)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  editar(bolsa: Bolsa): Observable<Bolsa>{
    return this.http.put<Bolsa>(this.basePath + bolsa.id, bolsa);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  deleteBolsa(id: number) {
    return this.http
      .delete<Bolsa>(this.basePath + id)
      .pipe(map(data => data));
  }

  agregarBolsa(bolsa:Bolsa): Observable<Bolsa>{
    const url_api = 'http://localhost:3000/api/bolsa/';
    return this.http.post<Bolsa>(environment.apiUrl+this.basePath1,bolsa);
  }
  saveBolsa(bolsa: Bolsa) {
    const url_api = 'http://localhost:3000/api/bolsa';
    return this.http
      .post<Bolsa>(url_api,bolsa)
      .pipe(map(data => data));
  }
}
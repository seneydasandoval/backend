export interface Parametrizacion {
    id: number,
    fecha_inicio: Date,
    fecha_fin: Date,
    duracion: number
}

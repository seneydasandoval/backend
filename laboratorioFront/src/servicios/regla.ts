export interface Regla {
    id: number,
    limite_inferior: number,
    limite_superior: number,
    monto_equivalente: number
}

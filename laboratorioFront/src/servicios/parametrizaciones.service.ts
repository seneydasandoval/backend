import { Parametrizacion } from './parametrizacion';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {environment} from "../environments/environment";
import { HttpParams, HttpClient } from '@angular/common/http';
import { map, retry, catchError } from "rxjs/operators";
import { Http, Response, Headers, RequestOptions } from '@angular/http';  

@Injectable({
  providedIn: 'root'
})
export class ParametrizacionesService {
  private basePath = 'http://localhost:3000/api/parametrizacion/';

  private headers = new Headers({ 'Content-Type': 'application/json' });  
  // Define API
  apiURL = 'http://localhost:3000/api';
  
  // For Using Fake API by Using It's URL  
  constructor(private http: HttpClient) {}
   
  private basePath1: string="/api/parametrizacion/";

  // HttpClient API get() method => Fetch employees list
   getClientes(): Observable<Parametrizacion> {
    return this.http.get<Parametrizacion>(this.apiURL + '/parametrizacion/')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  // HttpClient API get() method => Fetch employee
  getClienteById(id:number): Observable<Parametrizacion> {
    return this.http.get<Parametrizacion>(this.apiURL + '/parametrizacion/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  
  // HttpClient API put() method => Update employee
  updateCliente(id: number, parametrizacion: Parametrizacion): Observable<Parametrizacion> {
    return this.http.put<Parametrizacion>(this.apiURL + '/parametrizacion/' + id,parametrizacion)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  editar(parametrizacion: Parametrizacion): Observable<Parametrizacion>{
    return this.http.put<Parametrizacion>(this.basePath + parametrizacion.id, parametrizacion);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  deleteCliente(id: number) {
    return this.http
      .delete<Parametrizacion>(this.basePath + id)
      .pipe(map(data => data));
  }

  agregarCliente(parametrizacion:Parametrizacion): Observable<Parametrizacion>{
    const url_api = 'http://localhost:3000/api/parametrizacion/';
    return this.http.post<Parametrizacion>(environment.apiUrl+this.basePath1,parametrizacion);
  }
  saveCliente(parametrizacion: Parametrizacion) {
    const url_api = 'http://localhost:3000/api/parametrizacion';
    return this.http
      .post<Parametrizacion>(url_api,parametrizacion)
      .pipe(map(data => data));
  }


}
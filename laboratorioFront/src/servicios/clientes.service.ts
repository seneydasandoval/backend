import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {environment} from "../environments/environment";
import { HttpParams, HttpClient } from '@angular/common/http';
import { map, retry, catchError } from "rxjs/operators";
import { Cliente } from './cliente';
import { Http, Response, Headers, RequestOptions } from '@angular/http';    

@Injectable({
  providedIn: 'root'
})
export class ClientesService {
  private basePath = 'http://localhost:3000/api/cliente/';

  private headers = new Headers({ 'Content-Type': 'application/json' });  
  // Define API
  apiURL = 'http://localhost:3000/api';
  
  // For Using Fake API by Using It's URL  
  constructor(private http: HttpClient) {}
   
  private basePath1: string="/api/cliente/";

  // HttpClient API get() method => Fetch employees list
   getClientes(): Observable<Cliente> {
    return this.http.get<Cliente>(this.apiURL + '/cliente/')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  // HttpClient API get() method => Fetch employee
  getClienteById(id:number): Observable<Cliente> {
    return this.http.get<Cliente>(this.apiURL + '/cliente/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  
  // HttpClient API put() method => Update employee
  updateCliente(id: number, cliente: Cliente): Observable<Cliente> {
    return this.http.put<Cliente>(this.apiURL + '/cliente/' + id,cliente)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  editar(cliente: Cliente): Observable<Cliente>{
    return this.http.put<Cliente>(this.basePath + cliente.id, cliente);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  deleteCliente(id: number) {
    return this.http
      .delete<Cliente>(this.basePath + id)
      .pipe(map(data => data));
  }

  agregarCliente(cliente:Cliente): Observable<Cliente>{
    const url_api = 'http://localhost:3000/api/cliente/';
    return this.http.post<Cliente>(environment.apiUrl+this.basePath1,cliente);
  }
  saveCliente(cliente: Cliente) {
    const url_api = 'http://localhost:3000/api/cliente';
    return this.http
      .post<Cliente>(url_api,cliente)
      .pipe(map(data => data));
  }


}
import { Concepto } from './concepto';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {environment} from "../environments/environment";
import { HttpParams, HttpClient } from '@angular/common/http';
import { map, retry, catchError } from "rxjs/operators";
import { Http, Response, Headers, RequestOptions } from '@angular/http';    
@Injectable({
  providedIn: 'root'
})
export class ConceptosService {

  private basePath = 'http://localhost:3000/api/concepto/';

  private headers = new Headers({ 'Content-Type': 'application/json' });  
  // Define API
  apiURL = 'http://localhost:3000/api';
  
  // For Using Fake API by Using It's URL  
  constructor(private http: HttpClient) {}
   
  private basePath1: string="/api/concepto/";

  // HttpClient API get() method => Fetch employees list
   getConceptos(): Observable<Concepto> {
    return this.http.get<Concepto>(this.apiURL + '/concepto/')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  // HttpClient API get() method => Fetch employee
  getConceptoById(id:number): Observable<Concepto> {
    return this.http.get<Concepto>(this.apiURL + '/concepto/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  
  // HttpClient API put() method => Update employee
  updateConcepto(id: number, concepto: Concepto): Observable<Concepto> {
    return this.http.put<Concepto>(this.apiURL + '/concepto/' + id,concepto)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  editar(concepto: Concepto): Observable<Concepto>{
    return this.http.put<Concepto>(this.basePath + concepto.id, concepto);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  deleteConcepto(id: number) {
    return this.http
      .delete<Concepto>(this.basePath + id)
      .pipe(map(data => data));
  }

  agregarConcepto(concepto:Concepto): Observable<Concepto>{
    const url_api = 'http://localhost:3000/api/concepto/';
    return this.http.post<Concepto>(environment.apiUrl+this.basePath1,concepto);
  }
  saveConcepto(concepto: Concepto) {
    const url_api = 'http://localhost:3000/api/concepto';
    return this.http
      .post<Concepto>(url_api,concepto)
      .pipe(map(data => data));
  }

}

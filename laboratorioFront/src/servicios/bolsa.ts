export interface Bolsa {
    id: number,
    id_cliente: number,
    fecha_asignacion: Date,
    fecha_caducidad: Date,
    puntaje_asignado: number,
    puntaje_utilizado: number,
    saldo_puntos: number,
    monto_operacion: number
}
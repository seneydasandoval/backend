import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {environment} from "../environments/environment";
import { HttpParams, HttpClient } from '@angular/common/http';
import { map, retry, catchError } from "rxjs/operators";
import { Regla } from './regla';
import { Http, Response, Headers, RequestOptions } from '@angular/http';    

@Injectable({
  providedIn: 'root'
})
export class ReglasService {
  private basePath = 'http://localhost:3000/api/regla/';

  private headers = new Headers({ 'Content-Type': 'application/json' });  
  // Define API
  apiURL = 'http://localhost:3000/api';
  
  // For Using Fake API by Using It's URL  
  constructor(private http: HttpClient) {}
   
  private basePath1: string="/api/regla/";

  // HttpClient API get() method => Fetch employees list
   getReglas(): Observable<Regla> {
    return this.http.get<Regla>(this.apiURL + '/regla/')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  // HttpClient API get() method => Fetch employee
  getReglaById(id:number): Observable<Regla> {
    return this.http.get<Regla>(this.apiURL + '/regla/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  
  // HttpClient API put() method => Update employee
  updateRegla(id: number, regla: Regla): Observable<Regla> {
    return this.http.put<Regla>(this.apiURL + '/regla/' + id,regla)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  editar(regla: Regla): Observable<Regla>{
    return this.http.put<Regla>(this.basePath + regla.id, regla);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  deleteRegla(id: number) {
    return this.http
      .delete<Regla>(this.basePath + id)
      .pipe(map(data => data));
  }

  agregarRegla(regla:Regla): Observable<Regla>{
    const url_api = 'http://localhost:3000/api/regla/';
    return this.http.post<Regla>(environment.apiUrl+this.basePath1,regla);
  }
  saveRegla(regla: Regla) {
    const url_api = 'http://localhost:3000/api/regla';
    return this.http
      .post<Regla>(url_api,regla)
      .pipe(map(data => data));
  }


}
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from "@angular/material";
import { MatTableDataSource } from '@angular/material';
import { Cliente } from 'src/servicios/cliente';
import { ClientesService } from 'src/servicios/clientes.service'; 
import { Router } from '@angular/router';
 


@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {
  MyDataSource: any;  
  displayedColumns = ["nombre", "apellido", "nro_documento", "tipo_documento", "nacionalidad", "email",
  "telefono", "accion"];  
  @ViewChild(MatPaginator) paginator: MatPaginator;  
  @ViewChild(MatSort) sort: MatSort;  
  
  constructor(private router: Router, public dataService: ClientesService) { }  
  
  ngOnInit() {  
    this.RenderDataTable();  
 
  }  

  RenderDataTable() {  
    this.dataService.getClientes()  
      .subscribe(  
      res => {  
        this.MyDataSource = new MatTableDataSource();  
        this.MyDataSource.data = res;  
        this.MyDataSource.sort = this.sort;  
        this.MyDataSource.paginator = this.paginator;  
        console.log(this.MyDataSource.data);  
      },  
      error => {  
        console.log('Ocurrio un error al procesar los Clientes !!!' + error);  
      });  
  }  


  applyFilter(filterValue: string) {
    if(filterValue){
        this.MyDataSource.filter = filterValue.trim().toLowerCase();
    
      if (this.MyDataSource.paginator) {
        this.MyDataSource.paginator.firstPage();
      }
    }else{
        this.MyDataSource.filter= "";
    }
  }

  onDeleteCliente(id: number): void {
    if (confirm('Esta seguro que desea borrar al cliente?')) {
      this.dataService.deleteCliente(id).subscribe();
    }
    this.volverAlListado();
  }

  onclickAgregar(){
    this.router.navigate(['/cliente/crear']);
  }

  public volverAlListado(){
    this.router.navigate(['/cliente']);
  }
}

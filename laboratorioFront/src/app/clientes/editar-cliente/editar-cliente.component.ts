import { Component, OnInit } from '@angular/core';
import {  EventEmitter, Output, ViewChild } from '@angular/core';
import { Cliente } from 'src/servicios/cliente';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { ClientesService } from 'src/servicios/clientes.service';


@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.scss']
})
export class EditarClienteComponent implements OnInit {
  public form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private editarServicio: ClientesService,
    private recibir: ClientesService,
    private router: Router,
    ) { }
    ngOnInit() {
      const id = +this.route.snapshot.paramMap.get('id'); //Se obtiene el id de la ruta 
      //Para el servidor
      this.getClienteById(id);
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
  console.log( this.form.value);
   if (this.form.valid) {
     this.editarServicio.editar(
      this.form.value).subscribe(
       data => this.editadoCorrectamente(data),
       error => this.editadoIncorrecto(error));
    }
  }
  editadoCorrectamente(data: Cliente){
    console.log('Editado Correctamente');
    console.log(data);
    this.form.reset();
  }

  editadoIncorrecto(error){
    console.log('No se ha podido guardar los cambios. Error en el servidor!');
    console.log(error);
  }


  getClienteById(id: number) {
    this.recibir.getClienteById(id).subscribe(
      respuesta => {
        this.cargarFormulario(respuesta);
        console.log(respuesta);
      },
      error_respuesta => {
        console.log('Ha ocurrido un error al intentar cargar los datos del postulante');
        console.log(error_respuesta);
      }
      );
  }


  cargarFormulario(cliente: Cliente){
    this.form = this.formBuilder.group({
      id: new FormControl(cliente.id),
      nombre: new FormControl(cliente.nombre),
      apellido: new FormControl(cliente.apellido),
      nro_documento: new FormControl(cliente.nro_documento),
      tipo_documento: new FormControl(cliente.tipo_documento),
      nacionalidad: new FormControl(cliente.nacionalidad),
      email: new FormControl(cliente.email),
      telefono: new FormControl(cliente.telefono),
      fecha_nac: new FormControl(cliente.fecha_nac),
    });
  }


  onclickBack(){
    this.router.navigate(['/cliente']);
  }



}

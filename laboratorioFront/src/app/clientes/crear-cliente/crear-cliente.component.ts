import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ClientesService } from 'src/servicios/clientes.service';
import { Cliente } from 'src/servicios/cliente';
import { Router } from '@angular/router';
@Component({
  selector: 'app-crear-cliente',
  templateUrl: './crear-cliente.component.html',
  styleUrls: ['./crear-cliente.component.scss']
})
export class CrearClienteComponent implements OnInit {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private servicioAgregar: ClientesService,
    private router: Router) { }

  
  ngOnInit() {
    this.form = this.formBuilder.group({
        nombre: new FormControl(''),
        apellido: new FormControl(''),
        nro_documento: new FormControl(''),
        tipo_documento: new FormControl(''),
        nacionalidad: new FormControl(''),
        email: new FormControl(''),
        telefono: new FormControl(''),
        fecha_nac: new FormControl(''),
    });
  }
   public onSubmit() {
    console.log(this.form.value);
    this.servicioAgregar.agregarCliente(this.form.value).subscribe(
          data => this.recibidoCorrectamente(data),
          error=>this.errorRecibido(error)
        );
  } 

  public recibidoCorrectamente(data: Cliente){
    console.log("Creado "+data);
    this.volverAlListado();
  
  }
  public errorRecibido(error){
    console.log("se produjo un error ")
  }
  public volverAlListado(){
    this.router.navigate(['/cliente']);
  }
  

}

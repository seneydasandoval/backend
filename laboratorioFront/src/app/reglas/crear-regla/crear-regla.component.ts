import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ReglasService } from 'src/servicios/reglas.service';
import { Regla } from 'src/servicios/regla';
import { Router } from '@angular/router';
@Component({
  selector: 'app-crear-regla',
  templateUrl: './crear-regla.component.html',
  styleUrls: ['./crear-regla.component.scss']
})
export class CrearReglaComponent implements OnInit {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private servicioAgregar: ReglasService,
    private router: Router) { }

  
  ngOnInit() {
    this.form = this.formBuilder.group({
        limite_inferior: new FormControl(''),
        limite_superior: new FormControl(''),
        monto_equivalente: new FormControl(''),
    });
  }
   public onSubmit() {
    console.log(this.form.value);
    this.servicioAgregar.agregarRegla(this.form.value).subscribe(
          data => this.recibidoCorrectamente(data),
          error=>this.errorRecibido(error)
        );
  } 

  public recibidoCorrectamente(data: Regla){
    console.log("Creado "+data);
    this.volverAlListado();
  
  }
  public errorRecibido(error){
    console.log("se produjo un error ")
  }
  public volverAlListado(){
    this.router.navigate(['/regla']);
  }
  

}

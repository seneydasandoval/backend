import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearReglaComponent } from './crear-regla.component';

describe('CrearReglaComponent', () => {
  let component: CrearReglaComponent;
  let fixture: ComponentFixture<CrearReglaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearReglaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearReglaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from "@angular/material";
import { MatTableDataSource } from '@angular/material';
import { Regla } from 'src/servicios/regla';
import { ReglasService } from 'src/servicios/reglas.service'; 
import { Router } from '@angular/router';
 


@Component({
  selector: 'app-regla',
  templateUrl: './regla.component.html',
  styleUrls: ['./regla.component.scss']
})
export class ReglaComponent implements OnInit {
  MyDataSource: any;  
  displayedColumns = ["limite_inferior", "limite_superior", "monto_equivalente", "accion"];  
  @ViewChild(MatPaginator) paginator: MatPaginator;  
  @ViewChild(MatSort) sort: MatSort;  
  
  constructor(private router: Router, public dataService: ReglasService) { }  
  
  ngOnInit() {  
    this.RenderDataTable();  
 
  }  

  RenderDataTable() {  
    this.dataService.getReglas()  
      .subscribe(  
      res => {  
        this.MyDataSource = new MatTableDataSource();  
        this.MyDataSource.data = res;  
        this.MyDataSource.sort = this.sort;  
        this.MyDataSource.paginator = this.paginator;  
        console.log(this.MyDataSource.data);  
      },  
      error => {  
        console.log('Ocurrio un error al procesar los Reglas !!!' + error);  
      });  
  }  


  applyFilter(filterValue: string) {
    if(filterValue){
        this.MyDataSource.filter = filterValue.trim().toLowerCase();
    
      if (this.MyDataSource.paginator) {
        this.MyDataSource.paginator.firstPage();
      }
    }else{
        this.MyDataSource.filter= "";
    }
  }

  onDeleteRegla(id: number): void {
    if (confirm('Esta seguro que desea borrar al regla?')) {
      this.dataService.deleteRegla(id).subscribe();
    }
    this.volverAlListado();
  }

  onclickAgregar(){
    this.router.navigate(['/regla/crear']);
  }

  public volverAlListado(){
    this.router.navigate(['/regla']);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ClienteComponent } from '../app/clientes/cliente/cliente.component';
import { EditarClienteComponent } from '../app/clientes/editar-cliente/editar-cliente.component';
import { CrearClienteComponent } from '../app/clientes/crear-cliente/crear-cliente.component';
import { ReglaComponent } from '../app/reglas/regla/regla.component';
import { EditarReglaComponent } from '../app/reglas/editar-regla/editar-regla.component';
import { CrearReglaComponent } from '../app/reglas/crear-regla/crear-regla.component';

import { CrearConceptoComponent } from '../app/conceptos/crear-concepto/crear-concepto.component';
import { ConceptoComponent } from '../app/conceptos/concepto/concepto.component';
import { EditarConceptoComponent } from '../app/conceptos/editar-concepto/editar-concepto.component'; 

import { CrearBolsaComponent } from '../app/bolsas/crear-bolsa/crear-bolsa.component';
import { BolsaComponent } from '../app/bolsas/bolsa/bolsa.component';
import { CrearParametrizacionComponent } from './parametrizacion/crear-parametrizacion/crear-parametrizacion.component';
import { ParametrizacionComponent } from './parametrizacion/parametrizacion/parametrizacion.component';
import { EditarParametrizacionComponent } from './parametrizacion/editar-parametrizacion/editar-parametrizacion.component';
import { UsoComponent } from './uso/uso.component';
import { GastarComponent } from './gastar/gastar.component';

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: '', component: HomeComponent, children: 
    [
      {
        path:'cliente',
        children: [
          { path: 'crear', component:CrearClienteComponent },
          { path: '', component: ClienteComponent },
          { path: 'editar/:id', component: EditarClienteComponent},
          { path: 'ver/:id', component: ClienteComponent}
        ]
      },
      {
        path:'regla',
        children: [
          { path: 'crear', component:CrearReglaComponent },
          { path: '', component: ReglaComponent },
          { path: 'editar/:id', component: EditarReglaComponent},
          { path: 'ver/:id', component: ReglaComponent}
        ]
      },
      {
        path:'concepto',
        children: [
          { path: 'crear', component:CrearConceptoComponent },
          { path: '', component: ConceptoComponent },
          { path: 'editar/:id', component: EditarConceptoComponent},
        ]
      },
      {
        path:'bolsa',
        children: [
          { path: 'crear', component:CrearBolsaComponent },
          { path: '', component: BolsaComponent },
        ]
      },
      {
        path:'parametrizacion',
        children: [
          { path: 'crear', component:CrearParametrizacionComponent },
          { path: '', component: ParametrizacionComponent },
          { path: 'editar/:id', component:EditarParametrizacionComponent },
        ]
      },
      {
        path:'uso',
        children: [
          { path: '', component: UsoComponent },
        ]
      },
      {
        path:'cabecera',
        children: [
          { path: '', component: GastarComponent },
        ]
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

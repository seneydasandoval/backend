import { ParametrizacionesService } from './../../../servicios/parametrizaciones.service';
import { Component, OnInit } from '@angular/core';
import {  EventEmitter, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Parametrizacion } from 'src/servicios/parametrizacion';

@Component({
  selector: 'app-editar-parametrizacion',
  templateUrl: './editar-parametrizacion.component.html',
  styleUrls: ['./editar-parametrizacion.component.scss']
})
export class EditarParametrizacionComponent implements OnInit {
  public form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private editarServicio: ParametrizacionesService,
    private recibir: ParametrizacionesService,
    private router: Router,
    ) { }
    ngOnInit() {
      const id = +this.route.snapshot.paramMap.get('id'); //Se obtiene el id de la ruta 
      //Para el servidor
      this.getClienteById(id);
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
  console.log( this.form.value);
   if (this.form.valid) {
     this.editarServicio.editar(
      this.form.value).subscribe(
       data => this.editadoCorrectamente(data),
       error => this.editadoIncorrecto(error));
    }
  }
  editadoCorrectamente(data: Parametrizacion){
    console.log('Editado Correctamente');
    console.log(data);
    this.form.reset();
  }

  editadoIncorrecto(error){
    console.log('No se ha podido guardar los cambios. Error en el servidor!');
    console.log(error);
  }


  getClienteById(id: number) {
    this.recibir.getClienteById(id).subscribe(
      respuesta => {
        this.cargarFormulario(respuesta);
        console.log(respuesta);
      },
      error_respuesta => {
        console.log('Ha ocurrido un error al intentar cargar los datos del postulante');
        console.log(error_respuesta);
      }
      );
  }


  cargarFormulario(parametrizacion: Parametrizacion){
    this.form = this.formBuilder.group({
      id: new FormControl(parametrizacion.id),
      fecha_inicio: new FormControl(parametrizacion.fecha_inicio),
      fecha_fin: new FormControl(parametrizacion.fecha_fin),
      duracion: new FormControl(parametrizacion.duracion),

    });
  }


  onclickBack(){
    this.router.navigate(['/parametrizacion']);
  }


}

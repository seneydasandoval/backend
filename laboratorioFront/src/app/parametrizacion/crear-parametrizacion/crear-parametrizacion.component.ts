import { ParametrizacionesService } from './../../../servicios/parametrizaciones.service';
import { Parametrizacion } from './../../../servicios/parametrizacion';
import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-crear-parametrizacion',
  templateUrl: './crear-parametrizacion.component.html',
  styleUrls: ['./crear-parametrizacion.component.scss']
})
export class CrearParametrizacionComponent implements OnInit {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private servicioAgregar: ParametrizacionesService,
    private router: Router) { }

  
  ngOnInit() {
    this.form = this.formBuilder.group({
        fecha_inicio: new FormControl(''),
        fecha_fin: new FormControl(''),
        duracion: new FormControl(''),
    });
  }
   public onSubmit() {
    console.log(this.form.value);
    this.servicioAgregar.agregarCliente(this.form.value).subscribe(
          data => this.recibidoCorrectamente(data),
          error=>this.errorRecibido(error)
        );
  } 

  public recibidoCorrectamente(data: Parametrizacion){
    console.log("Creado "+data);
    this.volverAlListado();
  
  }
  public errorRecibido(error){
    console.log("se produjo un error ")
  }
  public volverAlListado(){
    this.router.navigate(['/parametrizacion']);
  }
  
}

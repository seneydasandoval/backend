import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearParametrizacionComponent } from './crear-parametrizacion.component';

describe('CrearParametrizacionComponent', () => {
  let component: CrearParametrizacionComponent;
  let fixture: ComponentFixture<CrearParametrizacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearParametrizacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearParametrizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { BolsasService } from '../../../servicios/bolsas.service';
import { Bolsa } from '../../../servicios/bolsa';
import { MatPaginator, MatSort } from "@angular/material";
import { MatTableDataSource } from '@angular/material'; 
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CoreService } from 'src/app/services/core.service';




@Component({
  selector: 'app-bolsa',
  templateUrl: './bolsa.component.html',
  styleUrls: ['./bolsa.component.scss']
})
export class BolsaComponent implements OnInit {
  bolsas;
  clientes;
  vencido;
  cantidad;
  idCliente;
  datos;
  
  MyDataSource: any;  
  displayedColumns = ["id_cliente", "fecha_asignacion","fecha_caducidad",
  "puntaje_asignado","puntaje_utilizado","saldo_puntos","monto_operacion","accion"];  
  @ViewChild(MatPaginator) paginator: MatPaginator;  
  @ViewChild(MatSort) sort: MatSort;  
  
  constructor(private _coreService:CoreService, private router: Router, public dataService: BolsasService) { }  
  
  ngOnInit() {  
    this.RenderDataTable();  
    //caba
    this.datos = {
      'idCliente':'',
      'cantidad':'',
      'vencido':''
    }
    this._coreService.get_bolsas(this.datos).subscribe(bolsas => {
      this.bolsas=bolsas;
      this._coreService.get_clientes().subscribe(clientes=>{
        this.clientes = clientes;
      })
    })
  }  

  filtrar() {
    console.log(this.datos)
    this._coreService.get_bolsas(this.datos).subscribe(bolsas => {
      this.bolsas=bolsas;
    });
  }

  resetear() {
    this.datos = {
      'idCliente':'',
      'cantidad':'',
      'vencido':''
    }
    this._coreService.get_bolsas(this.datos).subscribe(bolsas => {
      this.bolsas=bolsas;
    });
  }

  
  RenderDataTable() {  
    this.dataService.getBolsas()  
      .subscribe(  
      res => {  
        this.MyDataSource = new MatTableDataSource();  
        this.MyDataSource.data = res;  
        this.MyDataSource.sort = this.sort;  
        this.MyDataSource.paginator = this.paginator;  
        console.log(this.MyDataSource.data);  
      },  
      error => {  
        console.log('Ocurrio un error al procesar los bolsas !!!' + error);  
      });  
  }  


  applyFilter(filterValue: string) {
    if(filterValue){
        this.MyDataSource.filter = filterValue.trim().toLowerCase();
    
      if (this.MyDataSource.paginator) {
        this.MyDataSource.paginator.firstPage();
      }
    }else{
        this.MyDataSource.filter= "";
    }
  }

  onDeleteBolsa(id: number): void {
    if (confirm('Esta seguro que desea borrar el item?')) {
      this.dataService.deleteBolsa(id).subscribe();
      this.ngOnInit();
    }
    
  }

  onclickAgregar(){
    this.router.navigate(['/bolsa/crear']);
  }

  public volverAlListado(){
    this.router.navigate(['/bolsa']);
  }
}

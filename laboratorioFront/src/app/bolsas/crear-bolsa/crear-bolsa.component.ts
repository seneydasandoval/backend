import { BolsasService } from '../../../servicios/bolsas.service';
import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Bolsa } from 'src/servicios/bolsa';
import { Router } from '@angular/router';
import { CoreService } from 'src/app/services/core.service';

@Component({
  selector: 'app-crear-bolsa',
  templateUrl: './crear-bolsa.component.html',
  styleUrls: ['./crear-bolsa.component.scss']
})
export class CrearBolsaComponent implements OnInit {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private servicioAgregar: BolsasService,
    private router: Router) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
        id_cliente: new FormControl(''),
        monto_operacion: new FormControl(''),
    });
  }
  public onSubmit() {
    console.log(this.form.value);
    this.servicioAgregar.agregarBolsa(this.form.value).subscribe(
          data => this.recibidoCorrectamente(data),
          error=>this.errorRecibido(error)
        );
  } 

  public recibidoCorrectamente(data: Bolsa){
    console.log("Creado "+data);
    this.volverAlListado();
  
  }
  public errorRecibido(error){
    console.log("se produjo un error ")
  }
  public volverAlListado(){
    this.router.navigate(['/bolsa']);
  }
  

}

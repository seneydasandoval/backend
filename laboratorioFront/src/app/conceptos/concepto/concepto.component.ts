import { ConceptosService } from './../../../servicios/conceptos.service';
import { Concepto } from './../../../servicios/concepto';
import { MatPaginator, MatSort } from "@angular/material";
import { MatTableDataSource } from '@angular/material'; 
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';




@Component({
  selector: 'app-concepto',
  templateUrl: './concepto.component.html',
  styleUrls: ['./concepto.component.scss']
})
export class ConceptoComponent implements OnInit {
  MyDataSource: any;  
  displayedColumns = ["descripcion", "puntos", "accion"];  
  @ViewChild(MatPaginator) paginator: MatPaginator;  
  @ViewChild(MatSort) sort: MatSort;  
  
  constructor(private router: Router, public dataService: ConceptosService) { }  
  
  ngOnInit() {  
    this.RenderDataTable();  
 
  }  

  RenderDataTable() {  
    this.dataService.getConceptos()  
      .subscribe(  
      res => {  
        this.MyDataSource = new MatTableDataSource();  
        this.MyDataSource.data = res;  
        this.MyDataSource.sort = this.sort;  
        this.MyDataSource.paginator = this.paginator;  
        console.log(this.MyDataSource.data);  
      },  
      error => {  
        console.log('Ocurrio un error al procesar los conceptos !!!' + error);  
      });  
  }  


  applyFilter(filterValue: string) {
    if(filterValue){
        this.MyDataSource.filter = filterValue.trim().toLowerCase();
    
      if (this.MyDataSource.paginator) {
        this.MyDataSource.paginator.firstPage();
      }
    }else{
        this.MyDataSource.filter= "";
    }
  }

  onDeleteConcepto(id: number): void {
    if (confirm('Esta seguro que desea borrar el item?')) {
      this.dataService.deleteConcepto(id).subscribe();
      this.ngOnInit();
    }
    
  }

  onclickAgregar(){
    this.router.navigate(['/concepto/crear']);
  }

  public volverAlListado(){
    this.router.navigate(['/concepto']);
  }
}

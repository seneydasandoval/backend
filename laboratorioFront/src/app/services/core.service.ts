import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CoreService {

  BACKEND_URL = 'http://localhost:3000/api/';

  constructor( private http:HttpClient) { }

  get_usopuntos(datos) {
    let url = `${this.BACKEND_URL}cabecera`
    let sep = "?";
    
    for(let key in datos){
      if(datos[key] != ""){
        url += sep + key + "=" + datos[key];
        sep = '&';
      }
    }
    
    return this.http.get(url)
  }

  get_clientes() {
    const url = `${this.BACKEND_URL}cliente`
    return this.http.get(url)
  }

  get_conceptos() {
    const url = `${this.BACKEND_URL}concepto`
    return this.http.get(url)
  }

  get_bolsas(datos) {
    let url= `${this.BACKEND_URL}bolsa`;
    let sep = "?";
    
    for(let key in datos){
      if(datos[key] != ""){
        url += sep + key + "=" + datos[key];
        sep = '&';
      }
    }
    return this.http.get(url)
  }

  post_gastar(gasto) {
    const body = JSON.stringify(gasto);
    const headers = new HttpHeaders({
      'Content-Type':'application/json'
    })
    console.log(body);
    const url = `${this.BACKEND_URL}cabecera`;
    return this.http.post(url,body,{headers})
  }


}
